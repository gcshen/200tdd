package RestaurantFromYelp;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by gcshen on 14-2-19.
 */
public class RestaurantFromYelp {
    static String baseurl = "http://www.yelp.com/search/snippet?find_desc&find_loc=San Francisco,CA,USA&start=@&cflt=restaurants";
    static List<Restaurant_Yelp> restaurant_yelps = new ArrayList<Restaurant_Yelp>();

    public static void test() {
    }

    public static JSONObject getJson(String url) {
        try {
            Document doc = Jsoup.connect(url).ignoreContentType(true)
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36")
                    .referrer("http://www.yelp.com/search?cflt=restaurants&find_loc=San+Francisco,+CA,+USA")
                    .timeout(18000000)
                    .get();
            String json = doc.toString().substring(doc.toString().indexOf("{"), doc.toString().lastIndexOf("}") + 1);
            json = json.replaceAll("&quot;", "\"");
            JSONObject data = new JSONObject(json);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void stepone() {
        try {
            int i = 0;
            while (i < 991) {
                System.out.println("开始解析第"+(i/10+1)+"页数据！");
                String currenturl = baseurl.replaceAll("@", Integer.toString(i));
                JSONObject jsonObject = getJson(currenturl);
                JSONObject markers = jsonObject.getJSONObject("search_map").getJSONObject("markers");
                Iterator a = markers.keys();
                int count = markers.length();
                Document doc = Jsoup.parse(jsonObject.getString("search_results"));
                Element ul = doc.getElementsByClass("search-results").first();
                //Elements li_others=ul.getElementsByIndexGreaterThan(0);
                while (a.hasNext() && count-- > 1) {
                    try {
                        String num = a.next().toString();
                        JSONObject item = markers.getJSONObject(num);
                        Restaurant_Yelp restaurant_yelp = new Restaurant_Yelp();
                        restaurant_yelp.setUrl("http://www.yelp.com" + item.getString("url"));
                        restaurant_yelp.setLatitude(item.getJSONObject("location").getString("latitude"));
                        restaurant_yelp.setLongitude(item.getJSONObject("location").getString("longitude"));
                        Element li = ul.select("div[data-key=" + num + "]").first();
                        Elements all = li.getAllElements();
                        restaurant_yelp.setName(li.select("a[class=biz-name]").first().text());
                        if (all.hasClass("price-range"))
                            restaurant_yelp.setPricestar(li.getElementsByClass("price-range").first().text());
                        if (all.hasClass("category-str-list"))
                            restaurant_yelp.setCuisines(li.getElementsByClass("category-str-list").first().text());
                        if (all.hasClass("neighborhood-str-list"))
                            restaurant_yelp.setNeighborhood(li.getElementsByClass("neighborhood-str-list").first().text());
                        if (li.getElementsByTag("address") != null)
                            restaurant_yelp.setAddress(li.getElementsByTag("address").first().text());
                        if (all.hasClass("biz-phone"))
                            restaurant_yelp.setPhone(li.getElementsByClass("biz-phone").first().text());
                        restaurant_yelps.add(restaurant_yelp);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                System.out.println("正在保存第"+(i/10+1)+"页数据！");
                SaveEntities(restaurant_yelps);
                restaurant_yelps.clear();
                i += 10;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void SaveEntities(List<Restaurant_Yelp> entities)
    {
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("myJPA");
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        Session session=(Session)em.getDelegate();
        session.setFlushMode(FlushMode.MANUAL);
        for(int i=0;i<entities.size();i++)
        {
            session.save(entities.get(i));
        }
        session.flush();
        session.close();
        em.getTransaction().commit();
        emf.close();
    }
}