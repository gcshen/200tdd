package ScenceFromDZDP;

import ScenceFromBD.Scence_Phone;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by gcshen on 14-1-15.
 */
public class PhoneFromDZDP {
    private static ArrayList<Scence_Phone> scences = getNamesFromExcel();

    public static void test() {
        Pattern p = Pattern.compile("http://www.dianping.com/shop/[0-9]+");
        Matcher m = p.matcher("http://www.dianping.com/shop/656565");
        System.out.print(m.matches());
        stepone();
        steptwo();

    }

    private static void stepone() {
        try {
            String url = "http://www.baidu.com/s?wd=#+site:dianping.com&ie=utf-8";
            for (int i = 6044; i < scences.size(); i++) {
                System.out.println("第一步序号：" + i);
                String currenturl = url.replaceAll("#", scences.get(i).getName());
                Document doc = Jsoup.connect(currenturl).timeout(1800000).get();
                try {
                    Elements all = doc.getAllElements();
                    if (all.hasClass("result")) {
                        Element item = doc.getElementById("1");
                        scences.get(i).setUrl(item.getElementsByTag("a").first().attr("href"));
                    }
                } catch (Exception e) {
                    continue;
                }
            }
            //genExcel(scences);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("异常中断循环！");
        }
    }

    private static void steptwo() {
        for (int i = 6044; i < scences.size(); i++) {
            System.out.println("第二步序号：" + i);
            String url;
            try {
                url = scences.get(i).getUrl();
                Document doc = Jsoup.connect(url).referrer("http://www.baidu.com").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36").timeout(1800000).get();
                Pattern p = Pattern.compile("http://www.dianping.com/shop/[0-9]+");
                Matcher m = p.matcher(doc.location());
                if (m.find()) {
                    Element location = doc.getElementsByClass("shop-location").first();
                    scences.get(i).setPath(location.getElementsByClass("region").first().text() + location.getElementsByTag("span").get(1).text());
                    scences.get(i).setName_hanzi(doc.getElementsByClass("shop-title").first().text());
                    if (location.getElementsByClass("call").size() > 1)
                        scences.get(i).setPhone(location.getElementsByClass("call").first().text() + " " + location.getElementsByClass("call").get(1).text());
                    else
                        scences.get(i).setPhone(location.getElementsByClass("call").first().text());
                }
            } catch (Exception e) {

            }
        }
        genExcel(scences);
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("原始name")));
            sheet.addCell(new Label(1, 0, String.valueOf("名称匹配1")));
            sheet.addCell(new Label(2, 0, String.valueOf("路径1")));
            sheet.addCell(new Label(3, 0, String.valueOf("电话1")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_Phone> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\大众点评电话.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取大众点评电话结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取大众点评电话结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Phone sight = sights.get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName_hanzi())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getPhone()))));
                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Phone sight = sights.get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getName_hanzi())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf(sight.getPhone())));
                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Scence_Phone> getNamesFromExcel() {
        ArrayList<Scence_Phone> scences = new ArrayList<Scence_Phone>();
        String excelPath = "D:\\Users\\gcshen\\Desktop\\200TDD\\电话表.xls";
        File os;
        try {
            os = new File(excelPath);
            Workbook rwb = null;
            Sheet sheet = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
                sheet = rwb.getSheet("Sheet3");
            }
            int rowNumber = sheet.getRows();
            String currenturl;
            for (int i = 1; i < rowNumber; i++) {
                Scence_Phone bean = new Scence_Phone();
                String name = sheet.getColumn(3)[i].getContents();
                bean.setName(name);
                scences.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scences;
    }
}
