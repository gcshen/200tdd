package ScenceFromDZDP;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by gcshen on 14-1-15.
 */
public class Main {
    public static void main(String []args)
    {
        //PhoneFromDZDP.test();
        //OpenTimeFromDZDP.test();
        Restaurant_DZDP a=new Restaurant_DZDP();
        a.setName("aa");
        a.setPhone("bbbb");
        a.setAddress("cccc");
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("myJPA");
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(a);
        em.getTransaction().commit();
        em.clear();
        em.close();
        emf.close();
    }
}
