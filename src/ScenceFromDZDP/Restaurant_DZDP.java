package ScenceFromDZDP;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by gcshen on 14-2-18.
 */
@Entity(name="Restaurant_DZDP")
public class Restaurant_DZDP {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String name;
    private String id_dzdp;
    private String url;
    private String path;
    private String address;
    private String phone;
    private String opentime;
    private String description;
    private String avgprice;
    private String tastegrade;
    private String evirograde;
    private String servicegrade;
    private String star;
    private String tag;
    private String feature;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId_dzdp() {
        return id_dzdp;
    }

    public void setId_dzdp(String id_dzdp) {
        this.id_dzdp = id_dzdp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvgprice() {
        return avgprice;
    }

    public void setAvgprice(String avgprice) {
        this.avgprice = avgprice;
    }

    public String getTastegrade() {
        return tastegrade;
    }

    public void setTastegrade(String tastegrade) {
        this.tastegrade = tastegrade;
    }

    public String getEvirograde() {
        return evirograde;
    }

    public void setEvirograde(String evirograde) {
        this.evirograde = evirograde;
    }

    public String getServicegrade() {
        return servicegrade;
    }

    public void setServicegrade(String servicegrade) {
        this.servicegrade = servicegrade;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }
}
