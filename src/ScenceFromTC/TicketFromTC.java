package ScenceFromTC;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gcshen on 14-1-13.
 */
public class TicketFromTC {
    public static void test() {
        ArrayList<Scence_Ticket> scences = getNamesFromExcel();
        String url = "http://www.17u.cn/scenery/scenerysearchlist_0_0_#_0_0_0.html";
        String baseurl = "http://www.17u.cn/scenery/AjaxHelper/SceneryPriceHandler.ashx?action=GetSpnew&showCount=2&ids=#&isSimple=1&isShowAppThree=1&priceList=1&tabself=1&tabHotel=1&tabEatery=1&sceneryType=0&iid=0.43180282041430473";
        String currenturl;
        String currentbaseurl = "";
        try {
            for (int i = 0; i < scences.size(); i++) {
                System.out.println("序号：" + i);
                currenturl = url.replaceAll("#", scences.get(i).getName());
                Document doc = Jsoup.connect(currenturl).timeout(18000000).get();
                String[] urls = new String[2];
                String[] id = new String[2];
                String[] sname = new String[2];
                String[] paths = new String[2];
                String[] tickets = new String[2];
                try {
                    Elements all = doc.getAllElements();
                    if (all.hasClass("fir_name")) {
                        Elements items = doc.getElementsByClass("fir_name");
                        if (items.size() > 1) {
                            //第一条
                            currenturl = doc.getElementsByClass("fir_name").first().attr("href");
                            sname[0] = doc.getElementsByClass("fir_name").first().text();
                            id[0] = currenturl.substring(currenturl.indexOf("_") + 1, currenturl.indexOf("."));
                            urls[0] = "http://www.17u.cn" + currenturl;
                            paths[0] = doc.getElementsByClass("scenery_area").first().child(0).text();
                            currentbaseurl = baseurl.replaceAll("#", id[0]);
                            Document doc1 = Jsoup.connect(currentbaseurl).timeout(18000000).get();
                            tickets[0] = doc1.getElementsByClass("W02").get(0).text();


                            //第二条
                            currenturl = doc.getElementsByClass("fir_name").get(1).attr("href");
                            sname[1] = doc.getElementsByClass("fir_name").get(1).text();
                            id[1] = currenturl.substring(currenturl.indexOf("_") + 1, currenturl.indexOf("."));
                            urls[1] = "http://www.17u.cn" + currenturl;
                            paths[1] = doc.getElementsByClass("scenery_area").get(1).child(0).text();
                            currentbaseurl = baseurl.replaceAll("#", id[1]);
                            doc1 = Jsoup.connect(currentbaseurl).timeout(18000000).get();
                            tickets[1] = doc1.getElementsByClass("W02").get(0).text();

                            scences.get(i).setSname(sname);
                            scences.get(i).setId(id);
                            scences.get(i).setPaths(paths);
                            scences.get(i).setUrls(urls);
                            scences.get(i).setTickets(tickets);
                        } else if (items.size() > 0) {
                            //第一条
                            currenturl = doc.getElementsByClass("fir_name").first().attr("href");
                            sname[0] = doc.getElementsByClass("fir_name").first().text();
                            id[0] = currenturl.substring(currenturl.indexOf("_") + 1, currenturl.indexOf("."));
                            urls[0] = "http://www.17u.cn" + currenturl;
                            paths[0] = doc.getElementsByClass("scenery_area").first().child(0).text();
                            currentbaseurl = baseurl.replaceAll("#", id[0]);
                            doc = Jsoup.connect(currentbaseurl).timeout(18000000).get();
                            tickets[0] = doc.getElementsByClass("W02").get(0).text();
                            scences.get(i).setSname(sname);
                            scences.get(i).setId(id);
                            scences.get(i).setPaths(paths);
                            scences.get(i).setUrls(urls);
                            scences.get(i).setTickets(tickets);
                        }
                    }
                } catch (Exception e) {

                }
            }
            //genExcel(scences);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("异常中断循环！");
        }
        genExcel(scences);
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("name")));
            sheet.addCell(new Label(1, 0, String.valueOf("name1")));
            sheet.addCell(new Label(2, 0, String.valueOf("id1")));
            sheet.addCell(new Label(3, 0, String.valueOf("ticket1")));
            sheet.addCell(new Label(4, 0, String.valueOf("path1")));
            sheet.addCell(new Label(5, 0, String.valueOf("name2")));
            sheet.addCell(new Label(6, 0, String.valueOf("id2")));
            sheet.addCell(new Label(7, 0, String.valueOf("ticket2")));
            sheet.addCell(new Label(8, 0, String.valueOf("path2")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_Ticket> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\同程门票.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取同程门票结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取同程门票结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Ticket sight = sights.get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getSname()[0])));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getId()[0])));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTickets()[0])));
                        sheet.addCell(new Label(4, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPaths()[0])));
                        sheet.addCell(new Label(5, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getSname()[1])));
                        sheet.addCell(new Label(6, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getId()[1])));
                        sheet.addCell(new Label(7, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTickets()[1])));
                        sheet.addCell(new Label(8, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPaths()[1])));

                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Ticket sight = sights
                                .get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getSname()[0])));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getId()[0])));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf(sight.getTickets()[0])));
                        sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getPaths()[0])));
                        sheet.addCell(new Label(5, 1 + i, String.valueOf(sight.getSname()[1])));
                        sheet.addCell(new Label(6, 1 + i, String.valueOf(sight.getId()[1])));
                        sheet.addCell(new Label(7, 1 + i, String.valueOf(sight.getTickets()[1])));
                        sheet.addCell(new Label(8, 1 + i, String.valueOf(sight.getPaths()[1])));
                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Scence_Ticket> getNamesFromExcel() {
        ArrayList<Scence_Ticket> scences = new ArrayList<Scence_Ticket>();
        String excelPath = "D:\\Users\\gcshen\\Desktop\\门票表.xls";
        File os;
        try {
            os = new File(excelPath);
            Workbook rwb = null;
            Sheet sheet = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
                sheet = rwb.getSheet("Sheet3");
            }
            int rowNumber = sheet.getRows();
            String currenturl;
            for (int i = 1; i < rowNumber; i++) {
                Scence_Ticket bean = new Scence_Ticket();
                String name = sheet.getColumn(3)[i].getContents();
                bean.setName(name);
                scences.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scences;
    }
}
