package ScenceFromTC;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by gcshen on 14-1-17.
 */
@Entity
public class Scence_Comment_TC {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long comment_id;
    private String comment_detail;
    private String comment_service_star;
    private Date comment_time;
    private long comment_sightid;
    private Date spidertime;


    public long getComment_id() {
        return comment_id;
    }

    public void setComment_id(long comment_id) {
        this.comment_id = comment_id;
    }

    public String getComment_detail() {
        return comment_detail;
    }

    public void setComment_detail(String comment_detail) {
        this.comment_detail = comment_detail;
    }

    public String getComment_service_star() {
        return comment_service_star;
    }

    public void setComment_service_star(String comment_service_star) {
        this.comment_service_star = comment_service_star;
    }

    public Date getComment_time() {
        return comment_time;
    }

    public void setComment_time(Date comment_time) {
        this.comment_time = comment_time;
    }

    public long getComment_sightid() {
        return comment_sightid;
    }

    public void setComment_sightid(long comment_sightid) {
        this.comment_sightid = comment_sightid;
    }

    public Date getSpidertime() {
        return spidertime;
    }

    public void setSpidertime(Date spidertime) {
        this.spidertime = spidertime;
    }
}
