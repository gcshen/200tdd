package ScenceFromTC;

/**
 * Created by gcshen on 14-1-17.
 */
public class Scence_TC {
    private String sightid;
    private String name;
    private String ticketInfo;
    private String Trans;
    private String address;
    private String maplng;
    private String maplat;
    private String detail;
    private String opentime;
    private String sipdertime;
    private String url;
    private String ticketinfo_url;
    private String comment_url;
    private String tip_url;
    private String tip;
    private String level;
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSightid() {
        return sightid;
    }

    public void setSightid(String sightid) {
        this.sightid = sightid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTicketInfo() {
        return ticketInfo;
    }

    public void setTicketInfo(String ticketInfo) {
        this.ticketInfo = ticketInfo;
    }

    public String getTrans() {
        return Trans;
    }

    public void setTrans(String trans) {
        Trans = trans;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMaplng() {
        return maplng;
    }

    public void setMaplng(String maplng) {
        this.maplng = maplng;
    }

    public String getMaplat() {
        return maplat;
    }

    public void setMaplat(String maplat) {
        this.maplat = maplat;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getSipdertime() {
        return sipdertime;
    }

    public void setSipdertime(String sipdertime) {
        this.sipdertime = sipdertime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTicketinfo_url() {
        return ticketinfo_url;
    }

    public void setTicketinfo_url(String ticketinfo_url) {
        this.ticketinfo_url = ticketinfo_url;
    }

    public String getComment_url() {
        return comment_url;
    }

    public void setComment_url(String comment_url) {
        this.comment_url = comment_url;
    }

    public String getTip_url() {
        return tip_url;
    }

    public void setTip_url(String tip_url) {
        this.tip_url = tip_url;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }
}
