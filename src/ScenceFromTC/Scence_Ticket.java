package ScenceFromTC;

/**
 * Created by gcshen on 14-1-13.
 */
public class Scence_Ticket {
    private String[] urls = new String[2];
    private String[] id = new String[2];
    private String[] sname = new String[2];
    private String[] paths = new String[2];
    private String[] tickets = new String[2];
    private String name;

    public String[] getUrls() {
        return urls;
    }

    public void setUrls(String[] urls) {
        this.urls = urls;
    }

    public String[] getId() {
        return id;
    }

    public void setId(String[] id) {
        this.id = id;
    }

    public String[] getSname() {
        return sname;
    }

    public void setSname(String[] sname) {
        this.sname = sname;
    }

    public String[] getPaths() {
        return paths;
    }

    public void setPaths(String[] paths) {
        this.paths = paths;
    }

    public String[] getTickets() {
        return tickets;
    }

    public void setTickets(String[] tickets) {
        this.tickets = tickets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
