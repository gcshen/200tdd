package ScenceFromTC;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by gcshen on 14-1-17.
 */
public class AllFromTC {
    public static void test() {
        ArrayList<Scence_TC> scences = getNamesFromExcel();
        String url = "http://www.17u.cn/scenery/BookSceneryTicket_#.html";
        String ticket_url = "http://www.17u.cn/scenery/AjaxHelper/SceneryPriceFrame.aspx?action=GETSPNEW&showCount=0&ids=#&isSimple=1&isShowAppThree=1&sceneryType=20301&widthtype=1&iid=0.9159196610562503";
        String tip_url = "http://www.17u.cn/scenery/AjaxHelper/SceneryPriceFrame.aspx?action=GetSceneryBookKnows&id=#&sceneryType=20301&iid=0.6705827875994146";
        String currenturl = "";
        String currentticketurl = "";
        String currenttipurl = "";
        try {
            for (int i = 0; i < scences.size(); i++) {
                System.out.println("序号：" + i);
                //设置url值
                currenturl = url.replaceAll("#", scences.get(i).getSightid());
                currentticketurl = ticket_url.replaceAll("#", scences.get(i).getSightid());
                currenttipurl = tip_url.replaceAll("#", scences.get(i).getSightid());
                try {
                    Document doc = Jsoup.connect(currenturl).timeout(18000000).get();
                    //url赋值
                    scences.get(i).setUrl(currenturl);
                    scences.get(i).setTicketinfo_url(currentticketurl);
                    scences.get(i).setTip_url(currenttipurl);
                    scences.get(i).setSipdertime((new Date()).toString());
                    Elements all = doc.getAllElements();
                    String sightname = doc.getElementsByTag("h1").first().text();
                    scences.get(i).setName(sightname);
                    //景点星级
                    if (all.hasClass("scenicStar")) {
                        String level = doc.getElementsByClass("scenicStar").first().text();
                        scences.get(i).setLevel(level);
                    }
                    //地址
                    if (all.hasClass("positionAddress")) {
                        String address = doc.getElementsByClass("positionAddress").first().getElementsByTag("span").first().text();
                        scences.get(i).setAddress(address);
                    }
                    //交通和百度地图坐标
                    if (all.hasClass("traf_detail")) {
                        String trans = doc.getElementsByClass("traf_detail").first().text();
                        String maplng = doc.getElementById("mapLng").attr("value");
                        String maplat = doc.getElementById("mapLat").attr("value");
                        scences.get(i).setTrans(trans);
                        scences.get(i).setMaplat(maplat);
                        scences.get(i).setMaplng(maplng);
                    }
                    //游玩理由
                    if (all.hasClass("reason_ul")) {
                        String reason = "";
                        Elements reasons = doc.getElementsByClass("reason_ul").first().getElementsByTag("li");
                        for (int j = 0; j < reasons.size(); j++) {
                            if (reasons.get(j).children().size() > 1)
                                reason += reasons.get(j).getElementsByTag("span").first().text() + ":" + reasons.get(j).getElementsByTag("p").first().text() + "；";
                            else
                                reason += reasons.get(j).text();
                        }
                        scences.get(i).setReason(reason);
                    }
                    //详细介绍
                    if (all.hasClass("intro_information")) {
                        if (all.select("dl[class=intro_information]").size() == 1) {
                            String detail = "";
                            Elements infos = doc.getElementsByClass("intro_information").first().getElementsByTag("dt");
                            for (int j = 0; j < infos.size(); j++) {
                                if (infos.get(j).nextElementSibling() != null && infos.get(j).nextElementSibling().nodeName().equals("dd")) {
                                    detail += infos.get(j).text() + "：";
                                    detail += infos.get(j).nextElementSibling().text() + "；";
                                    Element second = infos.get(j).nextElementSibling().nextElementSibling();
                                    if (second != null)
                                        if (second.nodeName().equals("dd"))
                                            detail += infos.get(j).nextElementSibling().nextElementSibling().text();
                                }
                            }
                            scences.get(i).setDetail(detail);
                        } else {
                            String detail = "";
                            Elements infos = doc.getElementsByClass("intro_information");
                            for (int j = 0; j < infos.size(); j++) {
                                if (infos.get(j).getElementsByTag("dt").size() > 0) {
                                    detail += infos.get(j).getElementsByTag("dt").first().text() + "：";
                                    if (infos.get(j).getElementsByTag("dd").size() > 0)
                                        detail += infos.get(j).getElementsByTag("dd").first().text() + "：";
                                }
                            }
                            scences.get(i).setDetail(detail);
                        }
                    } else {
                        String detail = "";
                        scences.get(i).setDetail(doc.getElementsByClass("left_con").first().text());
                    }
                    //门票信息
                    Document doc_ticket = Jsoup.connect(currentticketurl).referrer(currenturl).userAgent("Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1").timeout(18000000).get();
                    Elements alltickets = doc_ticket.getAllElements();
                    if (alltickets.hasClass("api-piao")) {
                        Element adult = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=60301]").first();
                        Elements adultlist=null;
                        if (adult!=null&&adult.getAllElements().hasClass("piao-list"))
                            adultlist = adult.getElementsByClass("piao-list");
                        Element child = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=60302]").first();
                        Element home = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=60307]").first();
                        Element recommend = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=-100]").first();
                        String ticket = "";
                        if (adult != null) {
                            for (int t = 0; t < adultlist.size(); t++)
                                if (adultlist.get(t).getElementsByClass("W01").first().getElementsByClass("name").first() != null)
                                    ticket = adultlist.get(t).getElementsByClass("W01").first().getElementsByClass("name").first().text() + adultlist.get(t).getElementsByClass("W02").text() + "；";
                                else
                                    ticket = adultlist.get(t).getElementsByClass("W01").first().getElementsByClass("phoneRed").text() + adultlist.get(t).getElementsByClass("W02").text() + "；";
                        }
                        if (child != null) {
                            if (child.getElementsByClass("W01").first().getElementsByClass("name") != null)
                                ticket += child.getElementsByClass("W01").first().getElementsByClass("name").first().text() + child.getElementsByClass("W02").text() + "；";
                            else
                                ticket = child.getElementsByClass("W01").first().getElementsByClass("phoneRed").text();
                        }
                        if (home != null) {
                            if (home.getElementsByClass("W01").first().getElementsByClass("name") != null)
                                ticket += home.getElementsByClass("W01").first().getElementsByClass("name").first().text() + home.getElementsByClass("W02").text() + "；";
                            else
                                ticket = home.getElementsByClass("W01").first().getElementsByClass("phoneRed").text();
                        }
                        if (adult == null && child == null && home == null && recommend != null) {
                            Elements recommendtickets = recommend.getElementsByClass("piao-list");

                            for (int k = 0; k < recommendtickets.size(); k++) {
                                ticket += recommendtickets.get(k).getElementsByClass("W01").text() + "：" + recommendtickets.get(k).getElementsByClass("W02").text() + "；";
                            }
                        }
                        scences.get(i).setTicketInfo(ticket);
                    } else {
                        Elements ticket = doc.getElementsByClass("no_border");
                        if (ticket.size() > 0)
                            scences.get(i).setTicketInfo(ticket.first().text());
                    }
                    //开放时间和建议
                    Document doc_tip = Jsoup.connect(currenttipurl).referrer(currenturl).userAgent("Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1").timeout(18000000).get();
                    Elements tips = doc_tip.getAllElements();
                    if (tips.hasClass("left_con_b")) {
                        Elements tipdetails = doc_tip.getElementsByClass("left_con_b").get(0).getElementsByClass("left_con_r_b");
                        String opentimehead = tipdetails.first().getElementsByClass("left_con_float").get(0).text();
                        if (!opentimehead.equals("1.开放时间:")) {
                            int count = doc_tip.getElementsByClass("left_con_b").size();
                            if (count > 1)
                                tipdetails = doc_tip.getElementsByClass("left_con_b").get(1).getElementsByClass("left_con_r_b");
                        }
                        scences.get(i).setOpentime(tipdetails.first().getElementsByClass("left_con_float").get(1).text());
                        scences.get(i).setTip(tipdetails.last().getElementsByClass("left_con_float").get(1).text());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("异常中断循环！");
        }
        genExcel(scences);
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {

            sheet.addCell(new Label(0, 0, String.valueOf("sightid")));
            sheet.addCell(new Label(1, 0, String.valueOf("name")));
            sheet.addCell(new Label(2, 0, String.valueOf("ticketInfo")));
            sheet.addCell(new Label(3, 0, String.valueOf("Trans")));
            sheet.addCell(new Label(4, 0, String.valueOf("address")));
            sheet.addCell(new Label(5, 0, String.valueOf("maplng")));
            sheet.addCell(new Label(6, 0, String.valueOf("maplat")));
            sheet.addCell(new Label(7, 0, String.valueOf("detail")));
            sheet.addCell(new Label(8, 0, String.valueOf("opentime")));
            sheet.addCell(new Label(9, 0, String.valueOf("tip")));
            sheet.addCell(new Label(10, 0, String.valueOf("url")));
            sheet.addCell(new Label(11, 0, String.valueOf("ticketinfo_url")));
            sheet.addCell(new Label(12, 0, String.valueOf("comment_url")));
            sheet.addCell(new Label(13, 0, String.valueOf("tip_url")));
            sheet.addCell(new Label(14, 0, String.valueOf("sipdertime")));
            sheet.addCell(new Label(15, 0, String.valueOf("level")));
            sheet.addCell(new Label(16, 0, String.valueOf("reason")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_TC> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\同程景点匹配表.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取同程详情结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取同程详情结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_TC sight = sights.get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getSightid())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTicketInfo())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTrans())));
                        sheet.addCell(new Label(4, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getAddress())));
                        sheet.addCell(new Label(5, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getMaplng())));
                        sheet.addCell(new Label(6, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getMaplat())));
                        sheet.addCell(new Label(7, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getDetail())));
                        sheet.addCell(new Label(8, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getOpentime())));
                        sheet.addCell(new Label(9, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTip())));
                        sheet.addCell(new Label(10, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getUrl())));
                        sheet.addCell(new Label(11, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTicketinfo_url())));
                        sheet.addCell(new Label(12, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getComment_url())));
                        sheet.addCell(new Label(13, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTip_url())));
                        sheet.addCell(new Label(14, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getSipdertime())));
                        sheet.addCell(new Label(15, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getLevel())));
                        sheet.addCell(new Label(16, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getReason())));
                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_TC sight = sights.get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getSightid())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getTicketInfo())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf(sight.getTrans())));
                        sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getAddress())));
                        sheet.addCell(new Label(5, 1 + i, String.valueOf(sight.getMaplng())));
                        sheet.addCell(new Label(6, 1 + i, String.valueOf(sight.getMaplat())));
                        sheet.addCell(new Label(7, 1 + i, String.valueOf(sight.getDetail())));
                        sheet.addCell(new Label(8, 1 + i, String.valueOf(sight.getOpentime())));
                        sheet.addCell(new Label(9, 1 + i, String.valueOf(sight.getTip())));
                        sheet.addCell(new Label(10, 1 + i, String.valueOf(sight.getUrl())));
                        sheet.addCell(new Label(11, 1 + i, String.valueOf(sight.getTicketinfo_url())));
                        sheet.addCell(new Label(12, 1 + i, String.valueOf(sight.getComment_url())));
                        sheet.addCell(new Label(13, 1 + i, String.valueOf(sight.getTip_url())));
                        sheet.addCell(new Label(14, 1 + i, String.valueOf(sight.getSipdertime())));
                        sheet.addCell(new Label(15, 1 + i, String.valueOf(sight.getLevel())));
                        sheet.addCell(new Label(16, 1 + i, String.valueOf(sight.getReason())));
                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Scence_TC> getNamesFromExcel() {
        ArrayList<Scence_TC> scences = new ArrayList<Scence_TC>();
        String excelPath = "D:\\crawl\\result\\同程景点匹配.xls";
        File os;
        try {
            os = new File(excelPath);
            Workbook rwb = null;
            Sheet sheet = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
                sheet = rwb.getSheet("度假已匹配的数据");
            }
            int rowNumber = sheet.getRows();
            for (int i = 1; i < rowNumber; i++) {
                Scence_TC bean = new Scence_TC();
                String name = sheet.getColumn(0)[i].getContents();
                bean.setSightid(name);
                scences.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scences;
    }
}
