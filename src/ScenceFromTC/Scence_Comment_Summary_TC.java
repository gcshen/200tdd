package ScenceFromTC;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by gcshen on 14-2-20.
 */
@Entity(name="fx_octopus_result_detail_sight_tongcheng_second")
public class Scence_Comment_Summary_TC {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String sightname;
    private String sighturl;
    private String sightLevel;
    private int sightticketprice;
    private Date spidertime;
    private long taskbuildtime;
    public Scence_Comment_Summary_TC()
    {}
    public Scence_Comment_Summary_TC(String a,String b,String c,int d,Date e,long f)
    {
        this.sightname=a;
        this.sighturl=b;
        this.sightLevel=c;
        this.sightticketprice=d;
        this.spidertime=e;
        this.taskbuildtime=f;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSightname() {
        return sightname;
    }

    public void setSightname(String sightname) {
        this.sightname = sightname;
    }

    public String getSighturl() {
        return sighturl;
    }

    public void setSighturl(String sighturl) {
        this.sighturl = sighturl;
    }

    public String getSightLevel() {
        return sightLevel;
    }

    public void setSightLevel(String sightLevel) {
        this.sightLevel = sightLevel;
    }

    public int getSightticketprice() {
        return sightticketprice;
    }

    public void setSightticketprice(int sightticketprice) {
        this.sightticketprice = sightticketprice;
    }

    public Date getSpidertime() {
        return spidertime;
    }

    public void setSpidertime(Date spidertime) {
        this.spidertime = spidertime;
    }

    public long getTaskbuildtime() {
        return taskbuildtime;
    }

    public void setTaskbuildtime(long taskbuildtime) {
        this.taskbuildtime = taskbuildtime;
    }
}
