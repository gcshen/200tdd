package ScenceFromTC;

import ScenceFromBD.Scence;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by gcshen on 14-2-20.
 */
public class CommentFromTC {
    public static List<Scence_Comment_Summary_TC> scence_comment_summary_tcs = new ArrayList<Scence_Comment_Summary_TC>();
    public static List<Scence_Comment_TC> scence_comment_tcs = new ArrayList<Scence_Comment_TC>();
    public static Date baseTime = stringToDate("2013-01-01");

    public static void test() {
        //loadDataFromDB();
        scence_comment_summary_tcs.add(new Scence_Comment_Summary_TC("", "http://www.17u.cn/scenery/BookSceneryTicket_111.html", "", 1, new Date(), 1L));
        stepone();
    }

    public static void loadDataFromDB() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("myJPA");
        EntityManager em = factory.createEntityManager();
        //em.getTransaction().begin();
        //em.persist(new Scence_Comment_Summary_TC("打豆豆", "dd", "df", 0, new Date(), 1392799996362L));
        //em.getTransaction().commit();
        Query query = em.createQuery("select o from fx_octopus_result_detail_sight_tongcheng_second o ");
        scence_comment_summary_tcs = query.setFirstResult(0).setMaxResults(5).getResultList();
        em.close();
        factory.close();


    }

    public static void stepone() {
        String baseurl = "http://www.17u.cn/Scenery/AjaxHelper/AjaxCall.aspx?action=GetDPList&sort=1&mon=0&type=0&sId=@&page=#";
        for (int i = 0; i < scence_comment_summary_tcs.size(); i++) {
            try {
                String sightUrl = scence_comment_summary_tcs.get(i).getSighturl();
                System.out.println("正在解析点评：" + sightUrl);
                String sightid = sightUrl.substring(44, sightUrl.lastIndexOf('.'));
                String currenturl = baseurl.replaceAll("@", sightid);
                currenturl = currenturl.replaceAll("#", Integer.toString(1));
                JSONObject firstobject = getJson(currenturl, sightUrl);
                int pagecount = firstobject.getJSONObject("ShowList").getInt("PageCount");
                JSONArray firstdplist = firstobject.getJSONObject("ShowList").getJSONArray("DestinationCommentList");
                Boolean flag = true;
                //解析第一页
                System.out.println("正在解析点评：" + sightUrl + ",第1页");
                for (int j = 0; j < firstdplist.length() && flag; j++) {
                    JSONObject item = firstdplist.getJSONObject(j);
                    Date dptime = stringToDate(item.getString("DPTime"));
                    if (dptime.after(baseTime)) {
                        Scence_Comment_TC scence_comment_tc = new Scence_Comment_TC();
                        scence_comment_tc.setComment_detail(item.getString("DPContent"));
                        scence_comment_tc.setComment_service_star(item.getString("ServiceScore") + item.getString("ServiceGrade"));
                        scence_comment_tc.setComment_sightid(Long.parseLong(sightid));
                        scence_comment_tc.setComment_time(dptime);
                        scence_comment_tc.setSpidertime(new Date(System.currentTimeMillis()));
                        scence_comment_tcs.add(scence_comment_tc);
                    } else {
                        flag = false;
                        break;
                    }
                }
                //解析剩余的页
                for (int j = 2; j <= 20 && flag; j++) {
                    System.out.println("正在解析点评：" + sightUrl + ",第" + j + "页");
                    JSONObject jsonObject = getJson(baseurl.replace("@", sightid).replace("#", Integer.toString(j)), sightUrl);
                    JSONArray jsondplist = jsonObject.getJSONObject("ShowList").getJSONArray("DestinationCommentList");
                    for (int k = 0; k < jsondplist.length(); k++) {
                        JSONObject item = jsondplist.getJSONObject(k);
                        Date dptime = stringToDate(item.getString("DPTime"));
                        if (dptime.after(baseTime)) {
                            Scence_Comment_TC scence_comment_tc = new Scence_Comment_TC();
                            scence_comment_tc.setComment_detail(item.getString("DPContent"));
                            scence_comment_tc.setComment_service_star(item.getString("ServiceScore") + item.getString("ServiceGrade"));
                            scence_comment_tc.setComment_sightid(Long.parseLong(sightid));
                            scence_comment_tc.setComment_time(dptime);
                            scence_comment_tc.setSpidertime(new Date(System.currentTimeMillis()));
                            scence_comment_tcs.add(scence_comment_tc);
                        } else {
                            flag = false;
                            break;
                        }
                    }
                }
                //入库
                if (scence_comment_tcs.size() > 0) {
                    SaveEntitiesDB(scence_comment_tcs);
                    //SaveEntityExcle(scence_comment_tcs);
                    scence_comment_tcs.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("评论内容")));
            sheet.addCell(new Label(1, 0, String.valueOf("服务星级")));
            sheet.addCell(new Label(2, 0, String.valueOf("评论时间")));
            sheet.addCell(new Label(3, 0, String.valueOf("经典id")));
            sheet.addCell(new Label(4, 0, String.valueOf("爬取时间")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    public static void SaveEntityExcle(List<Scence_Comment_TC> entities) {
        String exclePath = "a.xls";
        WritableSheet sheet = null;
        WritableWorkbook wwb = null;
        Workbook wb = null;
        File file = new File(exclePath);
        try {
            if (!file.exists()) {
                wwb = Workbook.createWorkbook(file);
                sheet = wwb.createSheet("同程评论", 0);
                excelHeaders(sheet);
            } else {
                wb = Workbook.getWorkbook(file);
                wwb = Workbook.createWorkbook(file, wb);
                sheet = wwb.getSheet(0);
            }
            int rowNum = sheet.getRows();
            int columNum = sheet.getColumns();
            for (int i = 0; i < entities.size(); i++) {

                sheet.addCell(new Label(0, rowNum + i, entities.get(i).getComment_detail()));
                sheet.addCell(new Label(1, rowNum + i, entities.get(i).getComment_service_star()));
                sheet.addCell(new Label(2, rowNum + i, entities.get(i).getComment_time().toString()));
                sheet.addCell(new Label(3, rowNum + i, Long.toString(entities.get(i).getComment_sightid())));
                sheet.addCell(new Label(4, rowNum + i, entities.get(i).getSpidertime().toString()));

            }
            wwb.write();
            wwb.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void SaveEntitiesDB(List<Scence_Comment_TC> entities) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("myJPA");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Session session = (Session) em.getDelegate();
        session.setFlushMode(FlushMode.MANUAL);
        for (int i = 0; i < entities.size(); i++) {
            session.save(entities.get(i));
        }
        session.flush();
        session.close();
        em.getTransaction().commit();
        emf.close();
    }

    public static Date stringToDate(String dateStr) {
        Date dptime_format = new Date();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dptime_format = sdf.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dptime_format;
    }

    public static JSONObject getJson(String url, String refferUrl) {
        Document doc;
        String json = null;
        JSONObject data = null;
        try {
            doc = Jsoup.connect(url).referrer(refferUrl)
                    .ignoreContentType(true)
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36")
                    .timeout(18000000)
                    .get();
            json = doc.toString().substring(doc.toString().indexOf("{"), doc.toString().lastIndexOf("}") + 1);
            json = json.replaceAll("&quot;", "\"");
            data = new JSONObject(json);
            return data;
        } catch (Exception e) {
            json = json.replaceAll("\":\\{\"", "####");
            json = json.replaceAll("\":\"", "###");
            json = json.replaceAll("\",\"", "@@@");
            json = json.replaceAll("\\{\"", "@@");
            json = json.replaceAll("\":", "##");
            json = json.replaceAll(",\"", "#");
            json = json.replaceAll("\"\\}", "@");
            json = json.replaceAll("\"", "");
            json = json.replaceAll("####", "\":\\{\"");
            json = json.replaceAll("###", "\":\"");
            json = json.replaceAll("@@@", "\",\"");
            json = json.replaceAll("@@", "\\{\"");
            json = json.replaceAll("##", "\":");
            json = json.replaceAll("#", ",\"");
            json = json.replaceAll("@", "\"\\}");
            try {
                data = new JSONObject(json);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return data;
        }
    }
}
