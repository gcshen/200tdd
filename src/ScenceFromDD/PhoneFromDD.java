package ScenceFromDD;


import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gcshen on 14-1-13.
 */
public class PhoneFromDD {
    private static ArrayList<Scence_Phone_DD> scences = getNamesFromExcel();

    public static void test() {
        stepone();
        steptwo();

    }

    private static void stepone() {
        try {
            String url = "http://www.daodao.com/Search?q=#&ssrc=A";
            String currenturl;
            for (int i = 0; i < scences.size(); i++) {
                System.out.println("第一步序号：" + i);
                currenturl = url.replaceAll("#", scences.get(i).getName());
                Document doc = Jsoup.connect(currenturl).timeout(18000000).get();
                try {
                    Element results = doc.getElementById("results");
                    Elements items = results.getElementsByClass("search-item");
                    int count = items.size();
                    if (count > 1) {
                        scences.get(i).getUrls().add("http://www.daodao.com" + items.get(0).getElementsByTag("a").first().attr("href"));
                        scences.get(i).getName_hanzi().add(items.get(0).getElementsByTag("em").first().text());
                        scences.get(i).getPath().add(items.get(0).getElementsByClass("location").first().text());
                        scences.get(i).getUrls().add("http://www.daodao.com" + items.get(1).getElementsByTag("a").first().attr("href"));
                        scences.get(i).getName_hanzi().add(items.get(1).getElementsByTag("em").first().text());
                        scences.get(i).getPath().add(items.get(1).getElementsByClass("location").first().text());
                    } else if (count > 0) {
                        scences.get(i).getUrls().add("http://www.daodao.com" + items.get(0).getElementsByTag("a").first().attr("href"));
                        scences.get(i).getName_hanzi().add(items.get(0).getElementsByTag("em").first().text());
                        scences.get(i).getPath().add(items.get(0).getElementsByClass("location").first().text());
                    }
                } catch (Exception e) {
                    continue;
                }
            }
            //genExcel(scences);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("异常中断循环！");
        }
    }

    private static void steptwo() {
        for (int i = 0; i < scences.size(); i++) {
            System.out.println("第二步序号：" + i);
            String url;

            try {
                if (scences.get(i).getUrls().size() == 1) {
                    url = scences.get(i).getUrls().get(0);
                    Document doc = Jsoup.connect(url).timeout(18000000).get();
                    int t;
                    Elements all = doc.getAllElements();
                    boolean flag = all.hasClass("notLast");
                    if (flag) {
                        Elements s = doc.getElementsByClass("notLast");
                        Element g = s.get(0);
                        Elements ts = g.getElementsByTag("div");
                        String phone = ts.get(2).text();
                        scences.get(i).setPhones(new String[]{phone,""});
                    }
                } else if (scences.get(i).getUrls().size() > 1) {
                    String []phones=new String[2];
                    for (int j = 0; j < 2; j++) {
                        url = scences.get(i).getUrls().get(j);
                        Document doc = Jsoup.connect(url).timeout(18000000).get();
                        int t;
                        Elements all = doc.getAllElements();
                        boolean flag = all.hasClass("notLast");
                        if (flag) {
                            String phone = doc.getElementsByClass("notLast").get(0).getElementsByTag("div").get(2).text();
                            phones[j]=phone;
                        }
                    }
                    scences.get(i).setPhones(phones);
                }
            } catch (Exception e) {

            }
        }
        genExcel(scences);
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("原始name")));
            sheet.addCell(new Label(1, 0, String.valueOf("名称匹配1")));
            sheet.addCell(new Label(2, 0, String.valueOf("路径1")));
            sheet.addCell(new Label(3, 0, String.valueOf("电话1")));
            sheet.addCell(new Label(4, 0, String.valueOf("名称匹配2")));
            sheet.addCell(new Label(5, 0, String.valueOf("路径2")));
            sheet.addCell(new Label(6, 0, String.valueOf("电话2")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_Phone_DD> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\到到电话.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取到到电话结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取到到电话结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Phone_DD sight = sights.get(i);
                        if (sight.getUrls().size()> 1) {
                            sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                            sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getPhones())[0])));
                            sheet.addCell(new Label(4, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName_hanzi().get(1))));
                            sheet.addCell(new Label(5, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath().get(1))));
                            sheet.addCell(new Label(6, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getPhones())[1])));
                        } else if (sight.getUrls().size() > 0) {
                            sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                            sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getPhones())[0])));
                        }
                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Phone_DD sight = sights.get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        if (sight.getUrls().size() > 1) {
                            sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, 1 + i, String.valueOf((sight.getPhones())[0])));
                            sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getName_hanzi().get(1))));
                            sheet.addCell(new Label(5, 1 + i, String.valueOf(sight.getPath().get(1))));
                            sheet.addCell(new Label(6, 1 + i, String.valueOf((sight.getPhones())[1])));
                        } else if (sight.getUrls().size() > 0) {
                            sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, 1 + i, String.valueOf((sight.getPhones())[0])));
                        }

                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Scence_Phone_DD> getNamesFromExcel() {
        ArrayList<Scence_Phone_DD> scences = new ArrayList<Scence_Phone_DD>();
        String excelPath = "D:\\Users\\gcshen\\Desktop\\电话表.xls";
        File os;
        try {
            os = new File(excelPath);
            Workbook rwb = null;
            Sheet sheet = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
                sheet = rwb.getSheet("Sheet3");
            }
            int rowNumber = sheet.getRows();
            String currenturl;
            for (int i = 1; i < rowNumber; i++) {
                Scence_Phone_DD bean = new Scence_Phone_DD();
                String name = sheet.getColumn(3)[i].getContents();
                bean.setName(name);
                scences.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scences;
    }
}
