package ScenceFromDD;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gcshen on 14-2-18.
 */
public class QingMaiFromDD {
    static String baseurl = "http://www.daodao.com/RestaurantSearch?ajax=1&geo=293917&o=a#";
    static ArrayList<Scence_DD> qingmai_restaurant = new ArrayList<Scence_DD>();


    public static void test() {

        stepone();
        steptwo();

    }

    public static void stepone() {
        String currenturl = baseurl.replaceAll("#", "0");
        try {
            int pagenum = 0;
            System.out.println("请求第1页。");
            Document doc = Jsoup.connect(currenturl).referrer("http://www.daodao.com/Restaurants-g293917-Chiang_Mai.html").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36").timeout(18000000).get();
            try {
                Element pagecount = doc.getElementsByClass("pgDetail").first();
                int s = pagecount.text().indexOf('/');
                int e = pagecount.text().indexOf("页");
                pagenum = Integer.parseInt(pagecount.text().substring(s + 1, e));
                Elements eatery_list = doc.getElementsByClass("eatery-list");
                for (int i = 0; i < eatery_list.size(); i++) {
                    try {
                        System.out.println("第1页,第" + i + "项。");
                        Scence_DD item = new Scence_DD();
                        item.setId(eatery_list.get(i).id().substring(7));
                        item.setName(eatery_list.get(i).getElementsByClass("title").first().getElementsByTag("a").text());
                        item.setUrl("http://www.daodao.com" + eatery_list.get(i).getElementsByClass("title").first().getElementsByTag("a").attr("href"));
                        Elements all = eatery_list.get(i).getAllElements();
                        if (all.hasClass("eprice"))
                            item.setPrice(eatery_list.get(i).getElementsByClass("eprice").first().text());
                        if (all.hasClass("recommend"))
                            item.setRecommend(eatery_list.get(i).getElementsByClass("recommend").first().text());
                        if (all.hasClass("cuisine"))
                            item.setCuisines(eatery_list.get(i).getElementsByClass("cuisine").first().text());
                        if (all.hasClass("options"))
                            item.setOptions(eatery_list.get(i).getElementsByClass("options").first().text());
                        qingmai_restaurant.add(item);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        continue;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("第1页解析出错！");
            }

            for (int j = 1; j <pagenum ; j++) {
                currenturl = baseurl.replaceAll("#", Integer.toString(j * 15));
                System.out.println("请求第" +( j + 1) + "页。");
                doc = Jsoup.connect(currenturl).referrer("http://www.daodao.com/Restaurants-g293917-Chiang_Mai.html").userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36").timeout(18000000).get();
                try {
                    Elements eatery_list = doc.getElementsByClass("eatery-list");
                    for (int i = 0; i < eatery_list.size(); i++) {
                        try {
                            System.out.println("第" + j + "页，第" + i + "项。");
                            Scence_DD item = new Scence_DD();
                            item.setId(eatery_list.get(i).id().substring(7));
                            item.setName(eatery_list.get(i).getElementsByClass("title").first().getElementsByTag("a").text());
                            item.setUrl("http://www.daodao.com" + eatery_list.get(i).getElementsByClass("title").first().getElementsByTag("a").attr("href"));
                            Elements all = eatery_list.get(i).getAllElements();
                            if (all.hasClass("eprice"))
                                item.setPrice(eatery_list.get(i).getElementsByClass("eprice").first().text());
                            if (all.hasClass("recommend"))
                                item.setRecommend(eatery_list.get(i).getElementsByClass("recommend").first().text());
                            if (all.hasClass("cuisine"))
                                item.setCuisines(eatery_list.get(i).getElementsByClass("cuisine").first().text());
                            if (all.hasClass("options"))
                                item.setOptions(eatery_list.get(i).getElementsByClass("options").first().text());
                            qingmai_restaurant.add(item);
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            continue;
                        }
                    }
                } catch (Exception e1) {
                    System.out.println("第" + j + 1 + "页解析出错。");
                    e1.printStackTrace();
                    continue;
                }
            }
        } catch (Exception e) {
            System.out.println("请求出错！");
            e.printStackTrace();
        }
    }

    public static void steptwo() {
        for (int i = 0; i < qingmai_restaurant.size(); i++) {
            System.out.println("第二步，第" + i + "项");
            try {
                Document doc = Jsoup.connect(qingmai_restaurant.get(i).getUrl()).timeout(18000000).get();
                try {
                    Elements path = doc.getElementsByClass("crumbSep");
                    String path1 = "";
                    for (int j = 0; j < path.size() - 1; j++)
                        path1 += path.get(j).nextElementSibling().text() + ">";
                    qingmai_restaurant.get(i).setPath(path1);
                    Elements all = doc.getElementsByClass("infoBox").first().getAllElements();
                    if (all.hasClass("format_address")) {
                        qingmai_restaurant.get(i).setAddress(doc.getElementsByClass("format_address").text());
                    }
                    if (all.hasClass("sprite-greenPhone"))
                        qingmai_restaurant.get(i).setPhone(doc.getElementsByClass("sprite-greenPhone").first().nextElementSibling().text());
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.out.println("第" + i + "项餐馆解析出错！");
                }
            } catch (Exception e) {
                System.out.println("获取网页出错！");
                continue;
            }
        }
        genExcel(qingmai_restaurant);
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("餐馆名称")));
            sheet.addCell(new Label(1, 0, String.valueOf("ID")));
            sheet.addCell(new Label(2, 0, String.valueOf("地址")));
            sheet.addCell(new Label(3, 0, String.valueOf("电话")));
            sheet.addCell(new Label(4, 0, String.valueOf("价格")));
            sheet.addCell(new Label(5, 0, String.valueOf("菜系")));
            sheet.addCell(new Label(6, 0, String.valueOf("路径")));
            sheet.addCell(new Label(7, 0, String.valueOf("适合")));
            sheet.addCell(new Label(8, 0, String.valueOf("推荐")));
            sheet.addCell(new Label(9, 0, String.valueOf("URL")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_DD> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\抓取到到清迈餐馆结果.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取到到清迈餐馆结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取到到清迈餐馆结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_DD sight = sights.get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getId())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getAddress())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getPhone()))));
                        sheet.addCell(new Label(4, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPrice())));
                        sheet.addCell(new Label(5, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getCuisines())));
                        sheet.addCell(new Label(6, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(7, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getRecommend())));
                        sheet.addCell(new Label(8, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getOptions())));
                        sheet.addCell(new Label(9, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getUrl())));
                    }
                }
                else{
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_DD sight = sights.get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getId())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getAddress())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf((sight.getPhone()))));
                        sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getPrice())));
                        sheet.addCell(new Label(5, 1 + i, String.valueOf(sight.getCuisines())));
                        sheet.addCell(new Label(6, 1 + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(7, 1 + i, String.valueOf(sight.getRecommend())));
                        sheet.addCell(new Label(8,1 + i, String.valueOf(sight.getOptions())));
                        sheet.addCell(new Label(9, 1 + i, String.valueOf(sight.getUrl())));
                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }
}
