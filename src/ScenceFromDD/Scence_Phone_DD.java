package ScenceFromDD;

import java.util.ArrayList;

/**
 * Created by gcshen on 14-1-13.
 */
public class Scence_Phone_DD {
    private String name;
    private ArrayList<String> name_hanzi = new ArrayList<String>();
    private String[] phones = new String[2];
    private ArrayList<String> path = new ArrayList<String>();
    private ArrayList<String> urls = new ArrayList<String>();

    public String[] getPhones() {
        return phones;
    }

    public void setPhones(String[] phones) {
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getName_hanzi() {
        return name_hanzi;
    }

    public void setName_hanzi(ArrayList<String> name_hanzi) {
        this.name_hanzi = name_hanzi;
    }


    public ArrayList<String> getPath() {
        return path;
    }

    public void setPath(ArrayList<String> path) {
        this.path = path;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
    }
}
