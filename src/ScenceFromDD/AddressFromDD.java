package ScenceFromDD;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gcshen on 14-1-13.
 */
public class AddressFromDD {
    private static ArrayList<Scence_Address_DD> scences = getNamesFromExcel();

    public static void test() {
        stepone();
        steptwo();

    }

    private static void stepone() {
        try {
            String url = "http://www.daodao.com/Search?q=#&ssrc=A";
            String currenturl;
            for (int i = 0; i <scences.size(); i++) {
                System.out.println("第一步序号：" + i);
                currenturl = url.replaceAll("#", scences.get(i).getName());
                Document doc = Jsoup.connect(currenturl).timeout(18000000).get();
                try {
                    Element results = doc.getElementById("results");
                    Elements items = results.getElementsByClass("search-item");
                    int count = items.size();
                    if (count > 1) {
                        scences.get(i).getUrls().add("http://www.daodao.com" + items.get(0).getElementsByTag("a").first().attr("href"));
                        scences.get(i).getName_hanzi().add(items.get(0).getElementsByTag("em").first().text());
                        scences.get(i).getPath().add(items.get(0).getElementsByClass("location").first().text());
                        scences.get(i).getUrls().add("http://www.daodao.com" + items.get(1).getElementsByTag("a").first().attr("href"));
                        scences.get(i).getName_hanzi().add(items.get(1).getElementsByTag("em").first().text());
                        scences.get(i).getPath().add(items.get(1).getElementsByClass("location").first().text());
                    } else if (count > 0) {
                        scences.get(i).getUrls().add("http://www.daodao.com" + items.get(0).getElementsByTag("a").first().attr("href"));
                        scences.get(i).getName_hanzi().add(items.get(0).getElementsByTag("em").first().text());
                        scences.get(i).getPath().add(items.get(0).getElementsByClass("location").first().text());
                    }
                } catch (Exception e) {
                    continue;
                }
            }
            //genExcel(scences);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("异常中断循环！");
        }
    }

    private static void steptwo() {
        for (int i = 0; i < scences.size(); i++) {
            System.out.println("第二步序号：" + i);
            String url;
            try {
                if (scences.get(i).getUrls().size() == 1) {
                    url = scences.get(i).getUrls().get(0);
                    Document doc = Jsoup.connect(url).timeout(18000000).get();
                    Elements all = doc.getAllElements();
                    boolean flag = all.hasClass("format_address");
                    if (flag) {
                        Elements t = doc.getElementsByClass("format_address").first().getAllElements();
                        String address = doc.getElementsByClass("format_address").first().text();
                        if (t.hasClass("street-address")) {
                            String data = doc.getElementsByClass("street-address").first().data();
                            int a1 = data.indexOf('\'');
                            int a2 = data.indexOf('\'', a1 + 1);
                            int b1 = data.indexOf('\'', a2 + 1);
                            int b2 = data.indexOf('\'', b1 + 1);
                            int a3 = data.indexOf('\'', b2 + 1);
                            int a4 = data.indexOf('\'', a3 + 1);
                            int b3 = data.indexOf('\'', a4 + 1);
                            int b4 = data.indexOf('\'', b3 + 1);
                            int c1 = data.indexOf('\'', b4 + 1);
                            int c2 = data.indexOf('\'', c1 + 1);
                            String a = data.substring(a1 + 1, a2) + data.substring(a3 + 1, a4);
                            String b = data.substring(b1 + 1, b2) + data.substring(b3 + 1, b4);
                            String c = data.substring(c1 + 1, c2);
                            address += decodeUnicode(a + c + b);
                        }
                        scences.get(i).setAddresses(new String[]{address, ""});
                    }
                } else if (scences.get(i).getUrls().size() > 1) {
                    String[] addresses = new String[2];
                    for (int j = 0; j < 2; j++) {
                        url = scences.get(i).getUrls().get(j);
                        Document doc = Jsoup.connect(url).timeout(18000000).get();
                        Elements all = doc.getAllElements();
                        boolean flag = all.hasClass("format_address");
                        if (flag) {
                            String address = doc.getElementsByClass("format_address").first().text();
                            addresses[j] = address;
                            Elements t = doc.getElementsByClass("format_address").first().getAllElements();
                            if (t.hasClass("street-address")) {
                                String data = doc.getElementsByClass("street-address").first().data();
                                int a1 = data.indexOf('\'');
                                int a2 = data.indexOf('\'', a1 + 1);
                                int b1 = data.indexOf('\'', a2 + 1);
                                int b2 = data.indexOf('\'', b1 + 1);
                                int a3 = data.indexOf('\'', b2 + 1);
                                int a4 = data.indexOf('\'', a3 + 1);
                                int b3 = data.indexOf('\'', a4 + 1);
                                int b4 = data.indexOf('\'', b3 + 1);
                                int c1 = data.indexOf('\'', b4 + 1);
                                int c2 = data.indexOf('\'', c1 + 1);
                                String a = data.substring(a1 + 1, a2) + data.substring(a3 + 1, a4);
                                String b = data.substring(b1 + 1, b2) + data.substring(b3 + 1, b4);
                                String c = data.substring(c1 + 1, c2);
                                addresses[j] += decodeUnicode(a + c + b);
                            }
                        }
                        scences.get(i).setAddresses(addresses);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        genExcel(scences);
    }

    private static String decodeUnicode(String dataStr) {
        int start = 0;
        int end = 0;
        StringBuffer buffer = new StringBuffer();
        while (start > -1) {
            end = dataStr.indexOf("\\u", start + 2);
            String charStr = "";
            if (end == -1) {
                charStr = dataStr.substring(start + 2, dataStr.length());
            } else {
                charStr = dataStr.substring(start + 2, end);
            }
            char letter = (char) Integer.parseInt(charStr, 16);
            buffer.append(new Character(letter).toString());
            start = end;
        }
        return buffer.toString();
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("原始name")));
            sheet.addCell(new Label(1, 0, String.valueOf("名称匹配1")));
            sheet.addCell(new Label(2, 0, String.valueOf("路径1")));
            sheet.addCell(new Label(3, 0, String.valueOf("地址1")));
            sheet.addCell(new Label(4, 0, String.valueOf("名称匹配2")));
            sheet.addCell(new Label(5, 0, String.valueOf("路径2")));
            sheet.addCell(new Label(6, 0, String.valueOf("地址2")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_Address_DD> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\到到地址.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取到到地址结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取到到地址结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Address_DD sight = sights.get(i);
                        if (sight.getUrls().size() > 1) {
                            sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                            sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getAddresses())[0])));
                            sheet.addCell(new Label(4, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName_hanzi().get(1))));
                            sheet.addCell(new Label(5, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath().get(1))));
                            sheet.addCell(new Label(6, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getAddresses())[1])));
                        } else if (sight.getUrls().size() > 0) {
                            sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                            sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getAddresses())[0])));
                        }
                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Address_DD sight = sights.get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        if (sight.getUrls().size() > 1) {
                            sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, 1 + i, String.valueOf((sight.getAddresses())[0])));
                            sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getName_hanzi().get(1))));
                            sheet.addCell(new Label(5, 1 + i, String.valueOf(sight.getPath().get(1))));
                            sheet.addCell(new Label(6, 1 + i, String.valueOf((sight.getAddresses())[1])));
                        } else if (sight.getUrls().size() > 0) {
                            sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getName_hanzi().get(0))));
                            sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getPath().get(0))));
                            sheet.addCell(new Label(3, 1 + i, String.valueOf((sight.getAddresses())[0])));
                        }

                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Scence_Address_DD> getNamesFromExcel() {
        ArrayList<Scence_Address_DD> scences = new ArrayList<Scence_Address_DD>();
        String excelPath = "D:\\Users\\gcshen\\Desktop\\地址表.xls";
        File os;
        try {
            os = new File(excelPath);
            Workbook rwb = null;
            Sheet sheet = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
                sheet = rwb.getSheet("Sheet3");
            }
            int rowNumber = sheet.getRows();
            String currenturl;
            for (int i = 1; i < rowNumber; i++) {
                Scence_Address_DD bean = new Scence_Address_DD();
                String name = sheet.getColumn(3)[i].getContents();
                bean.setName(name);
                scences.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scences;
    }
}
