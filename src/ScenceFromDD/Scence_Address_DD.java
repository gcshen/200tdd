package ScenceFromDD;

import java.util.ArrayList;

/**
 * Created by gcshen on 14-1-13.
 */
public class Scence_Address_DD {
    private String name;
    private ArrayList<String> name_hanzi = new ArrayList<String>();
    private String[] addresses = new String[2];
    private ArrayList<String> path = new ArrayList<String>();
    private ArrayList<String> urls = new ArrayList<String>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getName_hanzi() {
        return name_hanzi;
    }

    public void setName_hanzi(ArrayList<String> name_hanzi) {
        this.name_hanzi = name_hanzi;
    }

    public String[] getAddresses() {
        return addresses;
    }

    public void setAddresses(String[] addresses) {
        this.addresses = addresses;
    }

    public ArrayList<String> getPath() {
        return path;
    }

    public void setPath(ArrayList<String> path) {
        this.path = path;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
    }
}
