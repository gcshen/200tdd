package ScenceFromBD;

/**
 * Created by gcshen on 14-1-9.
 */
public class Scence_OpenTime {
    private String name;
    private String name_hanzi;
    private String opentime;
    private String path;
    private String tcid;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTcid() {
        return tcid;
    }

    public void setTcid(String tcid) {
        this.tcid = tcid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_hanzi() {
        return name_hanzi;
    }

    public void setName_hanzi(String name_hanzi) {
        this.name_hanzi = name_hanzi;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
