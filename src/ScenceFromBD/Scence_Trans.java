package ScenceFromBD;

/**
 * Created by gcshen on 14-1-9.
 */
public class Scence_Trans {
    private String name;
    private String name_hanzi;
    private String trans;
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_hanzi() {
        return name_hanzi;
    }

    public void setName_hanzi(String name_hanzi) {
        this.name_hanzi = name_hanzi;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
