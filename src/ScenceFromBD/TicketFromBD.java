package ScenceFromBD;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gcshen on 14-1-9.
 */
public class TicketFromBD {
    public static void test() {
        ArrayList<Scence_Ticket> scences = getNamesFromExcel();
        try {
            String url = "http://lvyou.baidu.com/search?word=#&form=1&stype=scene";
            String baseurl = "http://lvyou.baidu.com/destination/ajax/allview?surl=#&format=ajax&cid=0&pn=@";
            String currenturl;
            String currentbaseurl;
            for (int i =361; i < scences.size(); i++) {
                System.out.println("序号："+i);
                currenturl = url.replaceAll("#", scences.get(i).getName());
                Document doc = Jsoup.connect(currenturl).timeout(18000000).get();
                try {
                    if (doc.location().equals(currenturl)) {
                        Elements h4 = doc.getElementsByTag("h4");
                        String pinyinurl = h4.first().getElementsByClass("nslog").first().attr("href");
                        String pinyin_name = pinyinurl.substring(pinyinurl.indexOf("/") + 1, pinyinurl.lastIndexOf("/"));
                        currentbaseurl = baseurl.replaceAll("#", pinyin_name);
                        JSONObject data=getJson(currentbaseurl);
                        JSONObject content =data.getJSONObject("content");
                        JSONObject ticket=content.getJSONObject("ticket_info");
                        scences.get(i).setName(pinyin_name);
                        scences.get(i).setTicket(ticket.getString("price_desc"));
                        scences.get(i).setName_hanzi(data.getString("sname"));
                        JSONArray jpath=data.getJSONArray("scene_path");
                        String path="";
                        for(int p=0;p<jpath.length();p++)
                        {
                            path+=","+((JSONObject)jpath.get(p)).getString("sname");
                        }
                        scences.get(i).setPath(path);
                    } else {
                        String pinyinurl = doc.getElementsByClass("titleheadname").first().attr("href");
                        String pinyin_name = pinyinurl.substring(pinyinurl.indexOf("/") + 1);
                        currentbaseurl = baseurl.replaceAll("#", pinyin_name);
                        JSONObject data=getJson(currentbaseurl);
                        JSONObject content =data.getJSONObject("content");
                        JSONObject ticket=content.getJSONObject("ticket_info");
                        scences.get(i).setName(pinyin_name);
                        scences.get(i).setTicket(ticket.getString("price_desc"));
                        scences.get(i).setName_hanzi(data.getString("sname"));
                        JSONArray jpath=data.getJSONArray("scene_path");
                        String path="";
                        for(int p=0;p<jpath.length();p++)
                        {
                            path+=","+((JSONObject)jpath.get(p)).getString("sname");
                        }
                        scences.get(i).setPath(path);
                    }
                    // genExcel(scences);
                } catch (Exception e) {
                    continue;
                }
            }
            //genExcel(scences);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("异常中断循环！");
        }
        genExcel(scences);
    }
    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("name")));
            sheet.addCell(new Label(1, 0, String.valueOf("ticket")));
            sheet.addCell(new Label(2, 0, String.valueOf("path")));
            sheet.addCell(new Label(3, 0, String.valueOf("name_hanzi")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }
    synchronized public static void genExcel(ArrayList<Scence_Ticket> sights) {
        File os = null;
        String excelPath ="D:\\crawl\\result\\门票.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取百度旅游门票结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取百度旅游门票结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Ticket sight = sights
                                .get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getTicket())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getName_hanzi())));

                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_Ticket sight = sights
                                .get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getTicket())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf(sight.getName_hanzi())));

                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }
    public static JSONObject getJson(String url) {
        try {
            Document doc = Jsoup.connect(url).timeout(18000000).get();
            String json=doc.toString().substring(doc.toString().indexOf("{"), doc.toString().lastIndexOf("}")+1);
            json=json.replaceAll("&quot;","\"");
            JSONObject data=new JSONObject(json).getJSONObject("data");

            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public static ArrayList<Scence_Ticket> getNamesFromExcel() {
        ArrayList<Scence_Ticket> scences = new ArrayList<Scence_Ticket>();
        String excelPath = "D:\\Users\\gcshen\\Desktop\\门票表.xls";
        File os;
        try {
            os = new File(excelPath);
            Workbook rwb = null;
            Sheet sheet = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
                sheet = rwb.getSheet("Sheet3");
            }
            int rowNumber = sheet.getRows();
            String currenturl;
            for (int i = 1; i < rowNumber; i++) {
                Scence_Ticket bean = new Scence_Ticket();
                String name = sheet.getColumn(3)[i].getContents();
                bean.setName(name);
                scences.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scences;
    }
}
