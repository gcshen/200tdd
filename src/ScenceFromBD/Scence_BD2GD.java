package ScenceFromBD;

/**
 * Created by gcshen on 14-1-8.
 */
public class Scence_BD2GD {
    private String name;
    private String map_x;
    private String map_y;
    private String gdmap_lng;
    private String gdmap_lat;

    public String getGdmap_lng() {
        return gdmap_lng;
    }

    public void setGdmap_lng(String gdmap_lng) {
        this.gdmap_lng = gdmap_lng;
    }

    public String getGdmap_lat() {
        return gdmap_lat;
    }

    public void setGdmap_lat(String gdmap_lat) {
        this.gdmap_lat = gdmap_lat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMap_x() {
        return map_x;
    }

    public void setMap_x(String map_x) {
        this.map_x = map_x;
    }

    public String getMap_y() {
        return map_y;
    }

    public void setMap_y(String map_y) {
        this.map_y = map_y;
    }
}
