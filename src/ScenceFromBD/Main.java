package ScenceFromBD;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.sql.DriverManager;

/**
 * Created by dhu on 13-12-28.
 */

public class Main {
    public static void main(String args[]) {
        zuobiaofrombaidu.test();
        //PlayTime.test();
        //BD2GD.test();
        //PhoneFromBD.test();
        //OpenTimeFromBD.test();
        TicketFromBD.test();
        //TransFromBD.test();
        //AddressFromBD.test();
    }




    public static void tongCheng_Test() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String sql = "insert into t(sightname,sightid,sightlevel,sightticketprice,sighturl,sightfatherurl,sightaddress,sighttransport,sightmaplng,sightmaplat) values( ";
            java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tongcheng_scenery?user=root&password=gcshen");
            java.sql.Statement st = con.createStatement();
            String url = "http://www.17u.cn/scenery/SearchList.aspx?&action=getlist&page=pageindex&kw=&pid=0&cid=0&cyid=0&theme=0&grade=0&money=0&sort=0&paytype=0&ismem=0&istuan=0&isnow=0&iid=0.08102992083877325";
            Document doc = Jsoup.connect(url).timeout(18000000).get();

            //第一步：循环建立任务，获取总页数，循环建立下次任务内从，每个任务解析10页内容，包括每个任务起止页，上级任务建立时间，此次爬取时间
            Element em = doc.getElementById("txt_AllpageNumber");
            String pageCount = em.val();
            System.out.println(pageCount);
            //第二步：获取每一页10个景点信息，名称，地址，级别，url（id）
            int pageStartIndex = 1, pageEndIndex = Integer.parseInt(pageCount);
            String currentUrl;
            String name = "";
            String sightUrl = "";
            String level = "";
            String price = "0";
            String id = "";
            String fatherUrl = "";
            String address = "";
            String transport;
            String mapLng;
            String mapLat;
            Elements ems;
            String exesql;
            long startTime = System.currentTimeMillis();
            for (int i = 2; i <= 2; i++) {
                System.out.println("第" + i + "页");
                currentUrl = url.replaceFirst("pageindex", Integer.toString(i));
                doc = Jsoup.connect(currentUrl).timeout(18000000).get();
                ems = doc.getElementsByClass("info_top");
                int t = 1;
                for (Element e : ems) {
                    try {
                        name = "";
                        sightUrl = "";
                        level = "";
                        address = "";
                        id = "";
                        price = "";
                        name = (e.getElementsByClass("fir_name")).first().text();//text()
                        sightUrl = "http://17u.cn" + (e.getElementsByClass("fir_name")).first().attr("href");
                        level = e.getElementsByClass("s_level").first().text();
                        price = e.getElementsByClass("s_price").get(0).getElementsByTag("b").text() == null ? "0" : e.getElementsByClass("s_price").get(0).getElementsByTag("b").text();

                        //System.out.println("名称："+name+"，级别："+level+"，票价："+price+"，id:"+id+"，景点url："+sightUrl);
                        //第三步，解析每一个景点详细信息:名称，id，url,父级目的地ur1，地址，开放时间?，门票，电话?，介绍?，交通，级别，游玩时长?,官网?，地图坐标
                        doc = Jsoup.connect(sightUrl).timeout(18000000).get();
                        id = doc.getElementById("hf_SceneryId").attr("value");
                        fatherUrl = "http://17u.cn" + doc.getElementsByClass("hty_nav").first().getElementsByTag("a").last().attr("href");
                        address = doc.getElementsByClass("positionAddress").first().getElementsByTag("span").text();
                        //Elements s=doc.getElementsByClass("left_con_b");
                        //Element openTime=s.get(1);
                        //String openTimeString=openTime.getElementsByTag("p").first().text();
                        transport = doc.getElementsByClass("traf_detail").first().text() == null ? "" : doc.getElementsByClass("traf_detail").first().text();
                        mapLng = doc.getElementById("mapLng").attr("value") == null ? "" : doc.getElementById("mapLng").attr("value");
                        mapLat = doc.getElementById("mapLat").attr("value") == null ? "" : doc.getElementById("mapLat").attr("value");
                        // System.out.println(name+","+id+","+level+","+price+","+sightUrl+","+fatherUrl+","+address+","+transport+","+mapLng+","+mapLat);
                        exesql = sql + "'" + name + "','" + id + "','" + level +
                                "'," + price + ",'" + sightUrl + "','" + fatherUrl + "','" + address + "','" + transport + "','" + mapLng + "','" + mapLat + "')";
                        st.execute(exesql);
                        t++;
                    } catch (Exception ex) {
                        System.out.println("第" + i + "页，第" + t + "条内容有误！");
                        exesql = "insert into t(sightname,sightlevel,sighturl,sightid,sightfatherurl) values ('" + name + "','" + level + "','" + sightUrl + "','" + id + "','" + fatherUrl + "')";
                        st.execute(exesql);
                        t++;
                        continue;
                    }
                }
            }
            long endTime = System.currentTimeMillis();
            System.out.println("一共执行了" + (endTime - startTime) / 1000f / 60f + "分钟");
            con.close();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
