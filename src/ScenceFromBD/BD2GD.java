package ScenceFromBD;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gcshen on 14-1-8.
 */
public class BD2GD {
    public static void test() {
        ArrayList<Scence_BD2GD> scences = getNamesFromExcel();
        try {
            String currenturl;
            for (int i = 0; i < scences.size(); i++) {
                String baseurl = "http://api.zdoz.net/bd2gcj.aspx?lat=#&lng=@";
                System.out.print("序号：" + i);
                try {
                    baseurl = baseurl.replaceAll("#", scences.get(i).getMap_x());
                    currenturl = baseurl.replaceAll("@", scences.get(i).getMap_y());
                    Document doc = Jsoup.connect(currenturl).timeout(18000000).get();
                    String json = doc.toString().substring(doc.toString().indexOf("{"), doc.toString().lastIndexOf("}") + 1);
                    json = json.replaceAll("&quot;", "\"");
                    JSONObject data = new JSONObject(json);
                    scences.get(i).setGdmap_lng(data.getString("Lng"));
                    scences.get(i).setGdmap_lat(data.getString("Lat"));
                    // genExcel(scences);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }
            //genExcel(scences);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("异常中断循环！");
        }
        genExcel(scences);
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("name")));
            sheet.addCell(new Label(1, 0, String.valueOf("map_x")));
            sheet.addCell(new Label(2, 0, String.valueOf("map_y")));
            sheet.addCell(new Label(3, 0, String.valueOf("gdmap_lng")));
            sheet.addCell(new Label(4, 0, String.valueOf("gdmap_lat")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_BD2GD> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\zuobiaobd2gd.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("百度转高德坐标结果");

            if (sheet == null) {
                sheet = wb.createSheet("百度转高德坐标结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_BD2GD sight = sights
                                .get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getMap_x())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getMap_y())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getGdmap_lng())));
                        sheet.addCell(new Label(4, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getGdmap_lat())));
                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_BD2GD sight = sights
                                .get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getMap_x())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getMap_y())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf(sight.getGdmap_lng())));
                        sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getGdmap_lat())));
                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getJson(String url) {
        try {
            Document doc = Jsoup.connect(url).timeout(18000000).get();
            String json = doc.toString().substring(doc.toString().indexOf("{"), doc.toString().lastIndexOf("}") + 1);
            json = json.replaceAll("&quot;", "\"");
            JSONObject data = new JSONObject(json).getJSONObject("data");

            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Scence_BD2GD> getNamesFromExcel() {
        ArrayList<Scence_BD2GD> scences = new ArrayList<Scence_BD2GD>();
        String excelPath = "D:\\Users\\gcshen\\Desktop\\zuobiaofrombaidu.xls";
        File os;
        try {
            os = new File(excelPath);
            Workbook rwb = null;
            Sheet sheet = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
                sheet = rwb.getSheet("抓取百度旅游坐标结果");
            }
            int rowNumber = sheet.getRows();
            String currenturl;
            for (int i = 1; i < rowNumber; i++) {
                Scence_BD2GD bean = new Scence_BD2GD();
                String name = sheet.getColumn(0)[i].getContents() + sheet.getColumn(1)[i].getContents();
                bean.setName(name);
                try {
                    String[] lnglat = sheet.getColumn(3)[i].getContents().split(",");
                    bean.setMap_x(lnglat[1]);
                    bean.setMap_y(lnglat[0]);
                } catch (Exception e) {
                }
                scences.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scences;
    }
}
