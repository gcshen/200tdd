package ScenceFromBD;

/**
 * Created by gcshen on 14-1-8.
 */
public class Scence {
    private String name;
    private String name_hanzi;
    private String url;
    private String map_info;
    private String map_x;
    private String map_y;
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName_hanzi() {
        return name_hanzi;
    }

    public void setName_hanzi(String name_hanzi) {
        this.name_hanzi = name_hanzi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMap_info() {
        return map_info;
    }

    public void setMap_info(String map_info) {
        this.map_info = map_info;
    }

    public String getMap_x() {
        return map_x;
    }

    public void setMap_x(String map_x) {
        this.map_x = map_x;
    }

    public String getMap_y() {
        return map_y;
    }

    public void setMap_y(String map_y) {
        this.map_y = map_y;
    }
}
