package ScenceFromBD;

/**
 * Created by gcshen on 14-1-8.
 */
public class Scence_PlayTime {
    private String name;
    private String name_hanzi;
    private String playtime;
    private String path;

    public String getName_hanzi() {
        return name_hanzi;
    }

    public void setName_hanzi(String name_hanzi) {
        this.name_hanzi = name_hanzi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaytime() {
        return playtime;
    }

    public void setPlaytime(String playtime) {
        this.playtime = playtime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
