package ScenceFromBD;

/**
 * Created by gcshen on 14-1-9.
 */
public class Scence_Phone {
    private String name;
    private String name_hanzi;
    private String phone;
    private String path;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_hanzi() {
        return name_hanzi;
    }

    public void setName_hanzi(String name_hanzi) {
        this.name_hanzi = name_hanzi;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
