package ScenceFromQY;

import ScenceFromDD.Scence_DD;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gcshen on 14-2-18.
 */
public class QingMaiFromQY {
    static String baseurl = "http://place.qyer.com/chiang-mai/food/page#";
    static ArrayList<Scence_QY> qingmai_restaurant = new ArrayList<Scence_QY>();

    public static void test() {
        stepone();
        steptwo();
        genExcel(qingmai_restaurant);
    }

    public static void stepone() {
        String currenturl = baseurl.replaceAll("#", "1");
        try {
            int pagenum = 0;
            System.out.println("请求第1页。");
            Document doc = Jsoup.connect(currenturl).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36").timeout(18000000).get();
            try {
                Element pagecount = doc.getElementsByClass("sum").first();
                int s = pagecount.text().indexOf('共');
                int e = pagecount.text().indexOf("条");
                pagenum = Integer.parseInt(pagecount.text().substring(s + 1, e)) / 15 + 1;
                Elements item = doc.getElementsByClass("pla_listpage_poilist").first().getElementsByTag("li");
                for (int i = 0; i < item.size(); i++) {
                    try {
                        Scence_QY sce = new Scence_QY();
                        Elements all = item.get(i).getAllElements();
                        if (all.hasClass("titcn")) {
                            sce.setUrl(item.get(i).getElementsByClass("titcn").first().getElementsByTag("a").attr("href"));
                            sce.setName(item.get(i).getElementsByClass("titcn").first().getElementsByTag("a").text());
                        } else if (all.hasClass("titen")) {
                            sce.setUrl(item.get(i).getElementsByClass("titen").first().getElementsByTag("a").attr("href"));
                            sce.setName(item.get(i).getElementsByClass("titen").first().getElementsByTag("a").text());
                        }
                        sce.setId(sce.getUrl().substring(26,sce.getUrl().lastIndexOf('/')));
                        qingmai_restaurant.add(sce);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        continue;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("第1页解析出错！");
            }
            //从第二页列表开始
            for (int j = 2; j <= pagenum; j++) {
                currenturl = baseurl.replaceAll("#", Integer.toString(j));
                System.out.println("请求第" + j  + "页。");
                doc = Jsoup.connect(currenturl).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36").timeout(18000000).get();
                try {
                    Elements item = doc.getElementsByClass("pla_listpage_poilist").first().getElementsByTag("li");
                    for (int i = 0; i < item.size(); i++) {
                        try {
                            System.out.println("第" + j + "页，第" + i + "项。");
                            Scence_QY sce = new Scence_QY();
                            Elements all = item.get(i).getAllElements();
                            if (all.hasClass("titcn")) {
                                sce.setUrl(item.get(i).getElementsByClass("titcn").first().getElementsByTag("a").attr("href"));
                                sce.setName(item.get(i).getElementsByClass("titcn").first().getElementsByTag("a").text());
                            } else if (all.hasClass("titen")) {
                                sce.setUrl(item.get(i).getElementsByClass("titen").first().getElementsByTag("a").attr("href"));
                                sce.setName(item.get(i).getElementsByClass("titen").first().getElementsByTag("a").text());
                            }
                            sce.setId(sce.getUrl().substring(26,sce.getUrl().lastIndexOf('/')));
                            qingmai_restaurant.add(sce);
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            continue;
                        }
                    }
                } catch (Exception e1) {
                    System.out.println("第" + j + 1 + "页解析出错。");
                    e1.printStackTrace();
                    continue;
                }
            }
        } catch (Exception e) {
            System.out.println("请求出错！");
            e.printStackTrace();
        }
    }


    public static void steptwo() {
        for (int i = 0; i < qingmai_restaurant.size(); i++) {
            System.out.println("第二步，第" + i + "项");
            try {
                Document doc = Jsoup.connect(qingmai_restaurant.get(i).getUrl()).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36").timeout(18000000).get();
                try {
                    Elements all = doc.getAllElements();
                    if (doc.getElementById("summary_box")!=null)
                        qingmai_restaurant.get(i).setSummary(doc.getElementById("summary_box").text());
                    if (all.hasClass("pla_textdetail_list")) {
                        Elements details = doc.getElementsByClass("pla_textdetail_list").first().children();
                        for (int j = 0; j < details.size(); j++) {
                            titlematch(details.get(j).getElementsByTag("span").first().text(), qingmai_restaurant.get(i), details.get(j).getElementsByTag("span").first().nextElementSibling().text());
                        }
                    }
                    if (all.hasClass("pla_textpage_tips"))
                        qingmai_restaurant.get(i).setTip(doc.getElementsByClass("pla_textpage_tips").first().child(1).text());
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.out.println("第" + i + "项餐馆解析出错！");
                    continue;
                }
            } catch (Exception e) {
                System.out.println("获取网页出错！");
                continue;
            }
        }
    }

    public static void titlematch(String title, Scence_QY sce, String value) {
        if (title.trim().equals("地址："))
            sce.setAddress(value);
        else if (title.trim().equals("到达方式："))
            sce.setArriveway(value);
        else if (title.trim().equals("人均消费："))
            sce.setPrice(value);
        else if (title.trim().equals("电话："))
            sce.setPhone(value);
        else if (title.trim().equals("所属分类："))
            sce.setCategory(value);
        else if (title.trim().equals("营业时间："))
            sce.setTime(value);
        else if (title.trim().equals("网址："))
            sce.setWebsite(value);
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("餐馆名称")));
            sheet.addCell(new Label(1, 0, String.valueOf("ID")));
            sheet.addCell(new Label(2, 0, String.valueOf("路径")));
            sheet.addCell(new Label(3, 0, String.valueOf("简介")));
            sheet.addCell(new Label(4, 0, String.valueOf("地址")));
            sheet.addCell(new Label(5, 0, String.valueOf("到达方式")));
            sheet.addCell(new Label(6, 0, String.valueOf("人均")));
            sheet.addCell(new Label(7, 0, String.valueOf("电话")));
            sheet.addCell(new Label(8, 0, String.valueOf("类别")));
            sheet.addCell(new Label(9, 0, String.valueOf("提示")));
            sheet.addCell(new Label(10, 0, String.valueOf("营业时间")));
            sheet.addCell(new Label(11, 0, String.valueOf("官网")));
            sheet.addCell(new Label(12, 0, String.valueOf("URL")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(ArrayList<Scence_QY> sights) {
        File os = null;
        String excelPath = "D:\\crawl\\result\\抓取穷游清迈餐馆结果.xls";
        String excelDir = "D:\\crawl\\result";
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取穷游清迈餐馆结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取穷游清迈餐馆结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_QY sight = sights.get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getId())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows() + i, String.valueOf((sight.getSummary()))));
                        sheet.addCell(new Label(4, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getAddress())));
                        sheet.addCell(new Label(5, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getArriveway())));
                        sheet.addCell(new Label(6, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPrice())));
                        sheet.addCell(new Label(7, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getPhone())));
                        sheet.addCell(new Label(8, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getCategory())));
                        sheet.addCell(new Label(9, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTip())));
                        sheet.addCell(new Label(10, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getTime())));
                        sheet.addCell(new Label(11, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getWebsite())));
                        sheet.addCell(new Label(12, rwb.getSheet(0).getRows() + i, String.valueOf(sight.getUrl())));
                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        Scence_QY sight = sights.get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getId())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getPath())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf((sight.getSummary()))));
                        sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getAddress())));
                        sheet.addCell(new Label(5, 1 + i, String.valueOf(sight.getArriveway())));
                        sheet.addCell(new Label(6, 1 + i, String.valueOf(sight.getPrice())));
                        sheet.addCell(new Label(7, 1 + i, String.valueOf(sight.getPhone())));
                        sheet.addCell(new Label(8, 1 + i, String.valueOf(sight.getCategory())));
                        sheet.addCell(new Label(9, 1 + i, String.valueOf(sight.getTip())));
                        sheet.addCell(new Label(10, 1 + i, String.valueOf(sight.getTime())));
                        sheet.addCell(new Label(11, 1 + i, String.valueOf(sight.getWebsite())));
                        sheet.addCell(new Label(12, 1 + i, String.valueOf(sight.getUrl())));
                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }
}
